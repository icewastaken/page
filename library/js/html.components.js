import * as html from "./html.js";
import { style } from "./html.env.js";

import { modal } from "../../src/components/modal.js";
import { button } from "../../src/components/button.js";

html.css(style.components('modal.css'));
html.css(style.components('button.css'));

html.css(style.components('color.css'));

export const htmlc = {
    modal,
    button,
};