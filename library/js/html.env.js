export const env = {
    api : (path, version = 1) => { return `api/v${version}/${path}` },
    pages : (path) => { return `src/pages/${path}` },
    components : (path) => { return `src/components/${path}` },
    module : (path) => { return `src/module/${path}` },
}

export const style = {
    pages : (path) => { return `style/pages/${path}` },
    components : (path) => { return `style/components/${path}` },
    module : (path) => { return `style/module/${path}` },
}

export const config = {
    creditWebpage : "Webpage made by: John Iceberg Velez",
    creditPhotograph : "Pictures credits and supplied by: Lamberto Planas",
    title : "City Information Office",
    shortDescription : "The technical and information arm of LGU Bislig.",
    logo : 'assets/bislig-logo-sm.png',
    home : {
        // NOTE: use 1:1 aspect ratio images
        picture : [
            "assets/t-division.jpg",
            "assets/it-division.png",
            "assets/pio-division.png",
            "",
        ]
    },
    about : {
        cover : "assets/cover.jpg",
        description : 'The City Information Office (CIO) is both the technical and information arm of LGU Bislig. It was previously called the Public Information Office (PIO) & was comprised of only 2 divisions before 2015, the Information Office and the Communications/Radio Office. By 2015, 2 divisions were added, the Tourism Office and the Information Technology Division (ITD) and the department became the City Information Office. The Public Employment Services Office (PESO) was added in 2016 and in 2018, the Communications/Radio Office was transferred to the City Risk Reduction Management Office (CDRRMO).',
        head : {
            name : 'Napoleon L. Velez',
            picture : '',
            position : "City Information Officer I",
        },
        divisions : {
            "Information Technology Division" : {
                picture : "assets/it-division.png",
                personnel : [
                    {
                        name : "Rose Lee L. Cancio",
                        picture : "",
                        position : "IT Officer I",
                        head : true,
                    },
                    {
                        name : "Aristotle Lloyd M. Cariaso",
                        picture : "",
                        position : "Administrative Aide I",
                    },
                    {
                        name : "Daisylyn B. Trocio",
                        picture : "",
                        position : "Administrative Aide I",
                    },
                    {
                        name : "Rodrigo Garay",
                        picture : "",
                        position : "Security Agent I",
                    },
                    {
                        name : "Carlo June Caimen",
                        picture : "",
                        position : "Computer Programmer II",
                    },
                    {
                        name : "Gary Zalde Carreon",
                        picture : "",
                        position : "Programmer",
                    },
                    {
                        name : "April Mae Feliscuzo",
                        picture : "",
                        position : "Programmer",
                    },
                    {
                        name : "Cyndie  Bensig",
                        picture : "",
                        position : "Computer Programmer Asst.",
                    },
                    {
                        name : "Jimmy Omangayon III",
                        picture : "assets/employees/itd/jimmy.jpg",
                        position : "Computer Tech Asst.",
                    },
                    {
                        name : "Ronald Gubaynon",
                        picture : "",
                        position : "Computer Tech Asst.",
                    },
                    {
                        name : "Lorenzo Orcino",
                        picture : "",
                        position : "Computer Tech Asst.",
                    },
                    {
                        name : "Jonathan Alcala",
                        picture : "",
                        position : "Computer Info Tech",
                    },
                    {
                        name : "Jolito C. Alameda",
                        picture : "",
                        position : "Lights & Sounds Operator",
                    }
                ]
            },
            "Public Information Office" : {
                picture : "assets/pio-division.png",
                personnel : [
                    {
                        name : "Laine S. Elechicon",
                        picture : "",
                        position : "Senior Labor and Employment Officer",
                        head : true,
                    },
                    {
                        name : "Lamberto S. Planas",
                        picture : "",
                        position : "Administrative Aide I",
                    }
                ],
            },
            "Tourism Office Division" : {
                picture : "assets/t-division.jpg",
                personnel : [
                    {
                        name : "Lorelei Teresa D. Lim",
                        picture : "",
                        position : "Tourism Operation Officer II",
                        head : true,
                    }
                ],
            },
            "Radio & Communication Division" : {
                picture : "",
                personnel : [
                    {
                        name : "Douglas Badilla",
                        picture : "",
                        position : "Communication Equipment Operator II",
                        head : true,
                    }
                ],
            },
        }
    },
    sysapp : [
        {
            name : "HRMIS HRMO",
            description : "Human Resource related data transactions and applications",
            picture : "assets/home/hrmis_hrmo.jpeg",
            link : "http://192.168.3.254:3000",
        },
        {
            name : "HRMIS Client",
            description : "Human Resource related data transactions and applications",
            picture : "assets/home/hrmis_client.jpeg",
            link : "http://192.168.3.254:4000",
            external : "http://122.52.210.202:4000",
        },
        {
            name : "E-Logbook",
            description : "Electronic logbook for incoming and outgoing generic documents",
            picture : "assets/home/e-logbook.jpeg",
            link : "http://192.168.3.254:8000",
            external : "http://122.52.210.202:8000",
        },
        {
            name : "ITT System",
            description : "Request ticketing and office inventory.",
            picture : "assets/home/it-support.jpeg",
            link : "http://192.168.3.254:1200",
            external : "http://bislig-itticketing.ap.ngrok.io",
        },
        {
            name : "DTS Requisitioner",
            description : "Creation, tracking, and archiving of transactional documents.",
            picture : "assets/home/dts.jpeg",
            link : "http://192.168.3.254:8001",
            external : "http://122.52.210.202",
        },
        {
            name : "DTS Budget Office",
            description : "Creation, tracking, and archiving of transactional documents.",
            picture : "assets/home/dts-budget.jpeg",
            link : "http://192.168.3.254:8100",
        },
        {
            name : "DTS Accounting Office",
            description : "Creation, tracking, and archiving of transactional documents.",
            picture : "assets/home/dts-accounting.jpeg",
            link : "http://192.168.3.254:8200",
        },
        {
            name : "DTS CMO & CVMO",
            description : "Creation, tracking, and archiving of transactional documents.",
            picture : "assets/home/dts-cmo-cvmo.jpeg",
            link : "http://192.168.3.254:8300",
        },
        {
            name : "DTS GSO",
            description : "Creation, tracking, and archiving of transactional documents.",
            picture : "assets/home/dts-gso.jpeg",
            link : "http://192.168.3.254:8400",
        },
        {
            name : "DTS Treasurer Office",
            description : "Creation, tracking, and archiving of transactional documents.",
            picture : "assets/home/dts-treasurer.jpeg",
            link : "http://192.168.3.254:8500",
        },
        {
            name : "SPMS",
        },
    ],
    default : {
        front : 'assets/home/default.jpeg',
        about : 'assets/home/default-about.jpeg',
        cover : 'assets/home/default-cover.jpeg',
        division : 'assets/home/default-division.jpeg',
        avatar : 'assets/home/default-avatar.png',
    }
}