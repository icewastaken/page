/*
    HTML.JS - A collection of javascript function that aids in creating HTML elements using javascript

    Author: Velez, John Iceberg
    Created: Febuary 28, 2022,
    Link: 
        - Facebook: fb.com/jonitalian
        - Contact: 09567047543
        - Email: icevelezsocial@gmail.com / jievelez@addu.edu.ph
*/

export const _name = "HTML.JS";

export const err = (message, { trace = false, log = false }) => {
    if (log == true) {
        console.log(`[${_name}]: ERR! ${message}`);
    } else if (trace == true) {
        console.trace(`[${_name}]: ERR! ${message}`);
    } else {
        alert(`[${_name}]: ERR! ${message}`);
        throw `[${_name}]: ERR! ${message}`;
    }
};

export const select = (select) => { return check(document.querySelector(select)); };
export const event = (element, event, callback) => { return check(element).addEventListener(event, () => callback()); };

export const check = (element) => {
    if (element === null) {
        err(`element is null or does not exsit!`, { trace : true })
        return document.createElement("error");
    };
    return element;
};

export const create = (tag = "error", attribute = "", children = "", _callback = "") => {

    if (typeof tag != "string" || tag == "") {
        let error = document.createElement("error");
        error.innerText = "HTML tag name error.";
        return error;
    }
    
    let element = document.createElement(tag);

    element.get = {
        id : (id) => { return check(document.getElementById(id)); },
        name : (name) => { return check(document.getElementsByName(name)); },
        tag : (tag) => { return check(document.getElementsByTagName(tag)); },
        class : (name) => { return check(document.getElementsByClassName(name)); },
    };
    
    element.attr = {
        add : (className) => { 
            className = className.split(" ");
            className.forEach(i => element.classList.add(i));
            return element;
        },
        remove : (className) => { 
            className = className.split(" ");
            className.forEach(i => element.classList.remove(i));
            return element;
        },
        toggle : (className) => { 
            element.classList.toggle(className);
            return element;
        },
        contains : (className) => { 
            element.classList.contains(className);
            return element;
        },
    };

    // children parameter check.
    if (typeof children == "function") {
        _callback = children;
        children = [];
    }
    // attribute parameter check
    if (typeof attribute == "function") {
        _callback = attribute;
        attribute = {};
    }
    if (typeof attribute !== "object") {
        element.innerText = attribute;
        attribute = {};
    }
    if (Array.isArray(attribute)) {
        children = attribute;
        attribute = {};
    }

    // append attribute object
    Object.keys(attribute).forEach((key, index) => {
        switch(key) {
            case "titleText":
                element.title = element.innerText = attribute[key];
            break;

            case "html": case "innerHTML": 
                element.innerHTML = attribute[key]; 
            break;

            case "text": case "innerText": 
                element.innerText = attribute[key]; 
            break;

            case "data":
                if (typeof attribute.data !== "object") return;
                Object.keys(attribute.data).forEach((key, index) => element.setAttribute(`data-${key}`, attribute.data[key]));
            break;

            case "event":
                element.addEventListener(attribute[key][0], (event) => attribute[key][1](event));
            break;

            default: 
                element.setAttribute(key, attribute[key]); 
            break;
        }   
    });

    // append childrean array 
    if (Array.isArray(children) && children.length > 0) children.forEach((item, index) => element.append(item));

    // check callback then call
    if (typeof _callback === "function") _callback(element);
    
    return element;
};

export const page = (url = "") => {
    if (url == "") return err("page parameter is empty.", { log : true });
    let page = create("script", { src : `${url}`, type : "module" });
    document.body.append(page);
    return page;
}

export const global = {};
export const body = create("div", { class : "_window_"});
export const modal = create("div", { class : "_modal_"});
export const toast = create("div", { class : "_toast_"});

export const params = (() => {
    let url = window.location.href.split("?"); 
    url = (url.length >= 2) ? url[1].split("&") : [];

    let object = {};

    url.forEach((item) => {
        let i = item.indexOf("=");
        item = [item.slice(0, i), item.slice(i+1)];
        object[item[0]] = item[1];
    });

    return object;
})();

export const render = (element, child) => {
    element.innerHTML = "";
    element.append(child);
};

export const initialize = () => {
    let script = [];

    document.body.childNodes.forEach(child => (child.nodeName == "SCRIPT") && script.push(child));
    document.body.innerHTML = "";

    script.forEach(i => document.body.append(i));

    document.body.prepend(body);
    document.body.prepend(modal);
    document.body.prepend(toast);
};

// OTHER FUNCTIONS

const listOfImportedCSS = [];
export const css = (url) => {
    if (listOfImportedCSS.indexOf(url) > -1) return err(`'${url}' has already been imported`, { log : true });
    document.head.append(create("link", { rel : "stylesheet", href : `${url}` }));
    listOfImportedCSS.push(url);
};

export const xhr = ({ url, options, type = '', trace = false }, { success = ()=>{}, error  = ()=>{
    err("xhr() response is not ok.", { trace : true });
}}) => {
    fetch(url, options).then(promise => {
        if (!promise.ok) {
            error(promise);
        } else {
            return (type == 'json') && promise.json() || ((type == 'blob') && promise.blob() || promise.text());
        }
    }).then(response => {
        success(response);
    }).catch((err) => {
        (trace == true) && console.trace(err);
    });
};

export const touchGesture = (element, {left = ()=>{}, right = ()=>{}, up = ()=>{}, down = ()=>{}, tap = ()=>{} }) => {
    let touchstartX, touchstartY;
    let touchendX, touchendY;

    element.addEventListener('touchstart', (event) => {
        touchstartX = event.changedTouches[0].screenX;
        touchstartY = event.changedTouches[0].screenY;
    }, {passive: true});
    
    element.addEventListener('touchend', (event) => {
        touchendX = event.changedTouches[0].screenX;
        touchendY = event.changedTouches[0].screenY;
        
        if (touchendY === touchstartY) tap(event);
        if (touchendX < touchstartX) left(event);
        if (touchendX > touchstartX) right(event);
        if (touchendY > touchstartY) up(event);
        if (touchendY < touchstartY) down(event);
        
    }, {passive: true});
};

export const randString = (length) => {
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;

    let result = '';
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};

export const capitalize = (string) => {
    if (!string) return '';
    string = string.toString();
    return string.charAt(0).toUpperCase() + string.slice(1);
};

/* not my code, credits to  - https://geraintluff.github.io/sha256/ */
export const sha256 = (ascii) => {
    function rightRotate(value, amount) {
        return (value>>>amount) | (value<<(32 - amount));
    };
    
    var mathPow = Math.pow;
    var maxWord = mathPow(2, 32);
    var lengthProperty = 'length'
    var i, j; // Used as a counter across the whole file
    var result = ''

    var words = [];
    var asciiBitLength = ascii[lengthProperty]*8;
    
    //* caching results is optional - remove/add slash from front of this line to toggle
    // Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
    // (we actually calculate the first 64, but extra values are just ignored)
    var hash = sha256.h = sha256.h || [];
    // Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
    var k = sha256.k = sha256.k || [];
    var primeCounter = k[lengthProperty];
    /*/
    var hash = [], k = [];
    var primeCounter = 0;
    //*/

    var isComposite = {};
    for (var candidate = 2; primeCounter < 64; candidate++) {
        if (!isComposite[candidate]) {
            for (i = 0; i < 313; i += candidate) {
                isComposite[i] = candidate;
            }
            hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
            k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
        }
    }
    
    ascii += '\x80' // Append Ƈ' bit (plus zero padding)
    while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
    for (i = 0; i < ascii[lengthProperty]; i++) {
        j = ascii.charCodeAt(i);
        if (j>>8) return; // ASCII check: only accept characters in range 0-255
        words[i>>2] |= j << ((3 - i)%4)*8;
    }
    words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
    words[words[lengthProperty]] = (asciiBitLength)
    
    // process each chunk
    for (j = 0; j < words[lengthProperty];) {
        var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
        var oldHash = hash;
        // This is now the undefinedworking hash", often labelled as variables a...g
        // (we have to truncate as well, otherwise extra entries at the end accumulate
        hash = hash.slice(0, 8);
        
        for (i = 0; i < 64; i++) {
            var i2 = i + j;
            // Expand the message into 64 words
            // Used below if 
            var w15 = w[i - 15], w2 = w[i - 2];

            // Iterate
            var a = hash[0], e = hash[4];
            var temp1 = hash[7]
                + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
                + ((e&hash[5])^((~e)&hash[6])) // ch
                + k[i]
                // Expand the message schedule if needed
                + (w[i] = (i < 16) ? w[i] : (
                        w[i - 16]
                        + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
                        + w[i - 7]
                        + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
                    )|0
                );
            // This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
            var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
                + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
            
            hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
            hash[4] = (hash[4] + temp1)|0;
        }
        
        for (i = 0; i < 8; i++) {
            hash[i] = (hash[i] + oldHash[i])|0;
        }
    }
    
    for (i = 0; i < 8; i++) {
        for (j = 3; j + 1; j--) {
            var b = (hash[i]>>(j*8))&255;
            result += ((b < 16) ? 0 : '') + b.toString(16);
        }
    }
    return result;
};