import * as html from "../../library/js/html.js";

export const button = (attribute, _callback = () => {}) => {

    let badge;
    let button = html.create("button", attribute, [
        html.create("div", { class : "badge off" }, (e) => badge = e)
    ]);
    button.state = false;

    button.on = () => {
        button.state = true;
        badge.attr.remove("off");
    }
    button.off = () => {
        badge.attr.add("off");
        button.state = false;
    }
    button.toggle = () => {
        badge.attr.toggle("off");
        button.state = !button.state;
    }

    _callback(button);

    return button;
}