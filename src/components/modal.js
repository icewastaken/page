import * as html from "../../library/js/html.js";

export const modal = (attribute, _callback = ()=>{}) => {

    (!attribute.animate && attribute.slide) && (attribute.animate = "down");
    attribute.slide = (attribute.slide == true) ? "slide" : "zoom";

    const opposite = { down : "up", up : "down", left : "right", right : "left" };
    
    const config = {
        animIn : ["animate__fadeIn", `animate__${attribute.slide}In${html.capitalize(attribute.animate)}`],
        animOut : ["animate__fadeOut", `animate__${attribute.slide}Out${html.capitalize(opposite[attribute.animate])}`],
    };

    const $ = {};

    const component = html.create("div", { class : `__modal animate__animated ${config.animIn[0]} ${(attribute.faster === true) ? 'animate__faster' : ''}` }, [
        html.create("div", { class : `animate__animated ${config.animIn[1]} ${(attribute.faster === true) ? 'animate__faster' : ''}` }, [attribute.card($)], (e) => {
            $.modal = e;
            (attribute.center || attribute.slide == "zoom") && $.modal.attr.add("center");
        })
    ])

    component.controller = (_callback) => _callback($);

    component.open = () => {
        if (attribute.blur) {
            html.body.attr.add("__modal_blur");
            html.toast.attr.add("__modal_blur");
            html.modal.childNodes.forEach(child => {
                child.childNodes[0].attr.add("__modal_blur");
            });
        }
        html.modal.append(component);
    }
    component.close = () => {
        if (attribute.blur) {
            html.body.attr.remove("__modal_blur");
            html.toast.attr.remove("__modal_blur");
            html.modal.childNodes.forEach(child => {
                child.childNodes[0].attr.remove("__modal_blur");
            });
        }
        component.classList.replace(`${config.animIn[0]}`, `${config.animOut[0]}`);
        $.modal.classList.replace(`${config.animIn[1]}`, `${config.animOut[1]}`);
        setTimeout(() => {
            html.modal.removeChild(component);
            component.classList.replace(`${config.animOut[0]}`, `${config.animIn[0]}`);
            $.modal.classList.replace(`${config.animOut[1]}`, `${config.animIn[1]}`);
        }, (attribute.faster === true) ? 300 : 800);
    }

    if (attribute.dismiss == true) component.addEventListener('click', event => component.close());

    return component;
};