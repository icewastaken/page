import * as html from "../../library/js/html.js";
import { htmlc } from "../../library/js/html.components.js";
import { env, style, config } from "../../library/js/html.env.js";

import * as parent from "../../index.js";

html.css(style.pages('index.about.css'));

export const $ = {
    div : {},
    input : {},
    text : {},
    button : {},
    carousel : {},
    modal : {},
    table : {},
};

export const model = {};

export const view = () => {

    $.div.person = (p) => {
        return html.create("div", [
            html.create("img", { src : `${(p.picture) ? p.picture : config.default.avatar}` }),
            html.create("div", [
                html.create("h1", `${p.name}`),
                html.create("h2", `${p.position}`),
            ])
        ]);
    };

    return html.create("div", { class : "_index_about" }, [
        html.create("div", { class : "about" }, [
            html.create("div", { class : "animate__animated animate__zoomIn animate__faster" }, [
                html.create("img", { src : `${(config.about.cover) ? config.about.cover : config.default.about}` }),
            ]),
            html.create("div", { class : "animate__animated animate__fadeInRight animate__faster" }, [
                html.create("h1", "About"),
                html.create("div", [
                    html.create("p", `${config.about.description}`),
                    html.create("button", { class : "__btn btn-primary mlr-2", text : "Information Systems"}, (e) => $.button.system = e),
                ])
            ]),
        ]),
        html.create("div", { class : "team animate__animated animate__zoomIn animate__faster" }, [
            html.create("h1", "Organization Structure"),
            html.create("div", { class : "head" }, [
                html.create("div", { class : "title" }, [
                    html.create("img", { src : `${(config.about.head.picture) ? config.about.head.picture : config.default.avatar}` }),
                    html.create("div", [
                        html.create("h1", `${config.about.head.name}`),
                        html.create("h2", `${config.about.head.position}`),
                    ])
                ]),
            ]),
            html.create("div", { class : "box"}, (e) => $.div.box = e)
        ]),
        html.create("div", { class : "flex-credit animate__animated animate__fadeInDown animate__faster" },[
            html.create("p", `${config.creditWebpage}`),
            html.create("p", `${config.creditPhotograph}`),
        ])
    ], () => controller($));
};

const controller = ({ modal, carousel, button, div, text, input, table }) => {
    button.system.addEventListener('click', event => parent.$.button.system.click());

    div.box.innerHTML = "";
    Object.keys(config.about.divisions).forEach(key => {
        const i = config.about.divisions[key], head = [], personnel = [];
        i.personnel.forEach(p => (p.head == true) && head.push(p) || personnel.push(p));
        const division = html.create("div", [
            html.create("div", [ html.create("img", { src : `${(i.picture) ? i.picture : config.default.division}`}), ]),
            html.create("h1", `${key}`),
        ], (e) => {        
            e.addEventListener('click', event => {
                htmlc.modal({
                    faster : true, dismiss : true,
                    card : () => {
                        return html.create("div", { class : "_index_about_modal"}, [
                            html.create("div", { class : "x" }, [ html.create("i", { class : "bi bi-x-lg"}) ]),
                            html.create("div", { class : "header"}, [ html.create("h1", key), ]),
                            html.create("div", { class : "content" }, [
                                html.create("div", { class : "head" }, (e) => head.forEach(h => e.append(div.person(h)))),
                                html.create("div", { class : "personnel" }, (e) => personnel.forEach(p => e.append(div.person(p)))),
                            ])
                        ]);
                    }
                }).open();
            });
        });
        div.box.append(division);
    })
}