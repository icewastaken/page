import * as html from "../../library/js/html.js";
import { env, style, config } from "../../library/js/html.env.js";

import * as parent from "../../index.js";

html.css(style.pages('index.home.css'));

export const $ = {
    div : {},
    input : {},
    text : {},
    button : {},
    carousel : {},
    modal : {},
    table : {},
};

export const model = {
}

export const view = () => {
    $.div.images = (i) => { return html.create("img", { src : `${(i) ? i : config.default.front}`}) };
    return html.create("div", { class : "_index_home" }, [
        html.create("div", { class : "welcome animate__animated animate__fadeInLeft animate__faster"}, [
            html.create("div", [
                html.create("h2", "Welcome to the"),
                html.create("h1", `${config.title}`),
                html.create("p", `${config.shortDescription}`),
                html.create("button", { class : "__btn btn-primary", text : "About"}, (e) => $.button.about = e)
            ])
        ]),
        html.create("div", { class : "images animate__animated animate__fadeInRight animate__faster"}, (e) => $.div.pictures = e),
    ], () => controller($));
};

const controller = ({ modal, carousel, button, div, text, input, table }) => {
    button.about.addEventListener('click', event => parent.$.button.about.click());
    config.home.picture.forEach(i => div.pictures.append(div.images(i)));
}