import * as html from "../../library/js/html.js";
import { env, style, config } from "../../library/js/html.env.js";

html.css(style.pages('index.system.css'));

export const $ = {
    div : {},
    input : {},
    text : {},
    button : {},
    carousel : {},
    modal : {},
    table : {},
};

export const model = {
}

export const view = () => {

    $.div.card = (i) => {
        return html.create("div", [
            html.create("img", { src : `${(i.picture) ? i.picture : config.default.cover}`}),
            html.create("h1", `${(i.name) ? i.name : 'No Name'}`),
            html.create("p", `${(i.description) ? i.description : 'No description available.'}`),
            html.create("button", { class : "__btn" }, (e) => {
                if (i.link && i.link.includes('http')) {
                    e.attr.add('btn-primary');
                    e.innerText = "Open Local";
                    e.addEventListener('click', event => window.open(`${i.link}`, '_blank'))
                } else {
                    e.attr.add('btn-secondary noclick');
                    e.innerText = "In Development";
                }
            }),
            html.create("button", { class : "__btn" }, (e) => {
                if (i.external && i.external.includes('http')) {
                    e.attr.add('btn-primary');
                    e.innerText = "Open External";
                    e.addEventListener('click', event => window.open(`${i.external}`, '_blank'))
                } else {
                    e.attr.add('btn-secondary noclick');
                    e.innerText = "Not Available";
                }
            }),
        ])
    };

    return html.create("div", { class : "_index_system" }, [
        html.create("h1", { text : "Information Systems", class : "animate__animated animate__fadeInDown animate__faster"}),
        html.create("div", { class : "animate__animated animate__zoomIn animate__faster" }, (e) => $.div.systems = e),
    ], () => controller($));
};

const controller = ({ modal, carousel, button, div, text, input, table }) => {
    config.sysapp.forEach(i => div.systems.append($.div.card(i)))
}