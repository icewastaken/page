// HTML JS LIBRARY
import * as html from "./library/js/html.js";
import { htmlc } from "./library/js/html.components.js";
import { env, config } from "./library/js/html.env.js";

import * as home from "./src/views/index.home.js";
import * as about from "./src/views/index.about.js";
import * as system from "./src/views/index.system.js";

export const $ = { 
    div : {}, 
    button : {}, 
    input : {}, 
    text : {},
    list : {},
};

export const model = {};

export const view = () => {
    return html.create("div", { class : "_homepage"}, [
        html.create("div", { class : "header margin" }, [
            html.create("div", { class : "container"}, [
                html.create("div", { class : "title" }, [
                    html.create("img", { src : `${config.logo}` }),
                    html.create("h1", `${config.title}`),
                ]),
                html.create("div", { class : "content" }, [
                    html.create("p", { class : "bi bi-x-lg icon" }, (e) => $.button.x = e),
                    html.create("p", { class : "active", text : "Home"}, (e) => $.button.home = e),
                    html.create("p", "About", (e) => $.button.about = e),
                    html.create("p", "Information Systems", (e) => $.button.system = e),
                ], (e) => $.div.nav = e),
                html.create("i", { class : "bi bi-list icon" }, (e) => $.button.icon = e)
            ])
        ]),
        html.create("div", { class : "body margin" }, [
            html.create("div", { class : "container"}, (e) => $.div.body = e)
        ]),
    ], () => controller($));
}

const controller = ({ div, button, input, text, list, pages }) => {
    
    const homePages = [home.view(), about.view(), system.view()];

    div.nav.childNodes.forEach((btn, index) => {
        if (index == 0) return;
        btn.addEventListener('click', event => {
            div.nav.childNodes.forEach(i => i.attr.remove('active'));
            event.target.attr.add('active');
            if (div.nav.attr.contains('active')) button.x.click();
            html.render(div.body, homePages[index-1]);
        });
    });

    button.x.addEventListener('click', event => {
        div.nav.attr.add('inactive');
        div.nav.attr.remove('active');
        setTimeout(() => div.nav.attr.remove('inactive'), 280);
    });

    button.icon.addEventListener('click', event => {
        div.nav.attr.add('active');
        div.nav.attr.remove('inactive');
    });

    button.home.click();
    if (html.params.p == 'about') button.about.click();
    if (html.params.p == 'apps') button.system.click();

    document.title = config.title;
};

html.initialize();
html.render(html.body, view());